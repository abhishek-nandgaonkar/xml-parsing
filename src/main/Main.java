package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
/*
 import java.io.File;

 public class Main {

 public static void main(String[] args)
 {

 File folder = new File("C:\\Users\\anandgaonkar\\Desktop\\SA-903\\playingWithPropertyTransfers\\MGW-Use-Cases\\test");
 File[] XMLFiles = folder.listFiles();
 HandlePropertyTransfers handlePropertyTransfers = new HandlePropertyTransfers();
 handlePropertyTransfers.start(XMLFiles);
 }

 }
 */
import org.xml.sax.InputSource;

public class Main {

	ArrayList<File> XMLFileList = new ArrayList();
	
	static File rootFolder = new File(
			"C:\\Users\\anandgaonkar\\Desktop\\Property Transfer Replacement\\OM-Automation - old\\OFFER-Prerequisites\\a");

	public static HandlePropertyTransfersNew handlePropertyTransfersNew;
	
	public Main()
	{
		//handlePropertyTransfersNew = new HandlePropertyTransfersNew();
	}
	
	public static void main(String[] args) throws IOException {

		extractFolderContent(rootFolder);
	}

	private static void extractFolderContent(File rootFolder) {
		
		File[] listOfFiles = rootFolder.listFiles();
		try {
			processListOfFiles(listOfFiles);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	private static void processListOfFiles(File[] listOfFiles) throws IOException {
		handlePropertyTransfersNew = new HandlePropertyTransfersNew();
		for (File file : listOfFiles) {
			if(file.isDirectory())
			{
				extractFolderContent(file);
			}
			else if (file.isFile() && getFileExtensionName(file).indexOf("xml") != -1) {
				System.out.println(file.getName());
				
				handlePropertyTransfersNew.processFile(file);
			}
		}
	}

	public static String getFileExtensionName(File f) {
		if (f.getName().indexOf(".") == -1) {
			return "";
		} else {
			return f.getName().substring(f.getName().length() - 3,
					f.getName().length());
		}
	}

}